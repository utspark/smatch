#include <stdio.h>
#include <stdbool.h>
#include "smatch.h"
#include "smatch_function_hashtable.h"

struct string_list * namespace_related_funcs = NULL;

static void match_expr(struct expression *expr)
{
	char * macro_name = get_macro_name(expr->pos);
	if(macro_name)
	{
		// We are using a macro!
		// We would now like to know if we are using EACCES or EPERM
		// const char clone_[] = "CLONE_";
		if(strncmp(macro_name, "CLONE_", 6) == 0)
		{
			char exact_loc[100];
			sprintf(exact_loc, "%s:%d:%d %s(..) references %s", get_filename(), expr->pos.line, expr->pos.pos, get_function(), macro_name);
			insert_string(&namespace_related_funcs, exact_loc);
		}
	}
}

static void end_file(struct expression *expr)
{
	if(ptr_list_size(namespace_related_funcs))
	{
		char *tmp;
		FOR_EACH_PTR(namespace_related_funcs, tmp) {
			sm_printf("%s\n", tmp);
		} END_FOR_EACH_PTR(tmp);
		__free_ptr_list(&namespace_related_funcs);
	}
}

void check_namespace_related(int id)
{
	add_hook(&match_expr, EXPR_HOOK);
	add_hook(&end_file, END_FILE_HOOK);
}
