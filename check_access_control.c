#include <stdio.h>
#include <stdbool.h>
#include "smatch.h"
#include "smatch_function_hashtable.h"

struct string_list * access_ctl_funcs = NULL;

static void match_expr(struct expression *expr)
{
	char * macro_name = get_macro_name(expr->pos);
	if(macro_name)
	{
		// We are using a macro!
		// We would now like to know if we are using EACCES or EPERM
		if((strcmp(macro_name, "EACCES") == 0) || (strcmp(macro_name, "EPERM") == 0))
		{
			char exact_loc[100];
			sprintf(exact_loc, "%s:%d:%d %s(..) references %s", get_filename(), expr->pos.line, expr->pos.pos, get_function(), macro_name);
			insert_string(&access_ctl_funcs, exact_loc);
		}
	}
}

static void end_file(struct expression *expr)
{
	if(ptr_list_size(access_ctl_funcs))
	{
		char *tmp;
		FOR_EACH_PTR(access_ctl_funcs, tmp) {
			sm_printf("%s\n", tmp);
		} END_FOR_EACH_PTR(tmp);
		__free_ptr_list(&access_ctl_funcs);
	}
}

void check_access_control(int id)
{
	add_hook(&match_expr, EXPR_HOOK);
	add_hook(&end_file, END_FILE_HOOK);
}
