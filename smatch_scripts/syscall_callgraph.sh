#!/bin/bash

# Usage: Run this script in the same directory as your SQLite database file
#        It will create a directory and fill it with text files, each named after
#        and containing the call graph of one system call.

SMATCH_DIR=$(dirname "$0")/..
OUTPUT_DIR=./syscall_callgraphs
SYSCALL_LIST=$SMATCH_DIR/smatch_scripts/systemcall_list.txt

mkdir -p $OUTPUT_DIR

while read line; do
  echo $line
  $SMATCH_DIR/smatch_data/db/smdb.py desc_calls __se_sys_$line > $OUTPUT_DIR/$line.txt
done <$SYSCALL_LIST

