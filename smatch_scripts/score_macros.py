import argparse
from pprint import pprint
import re
import json

# https://regex101.com/r/xQjpEj/1
access_ctl_re = re.compile(
    r"^(?P<file>\S+):\d+:\d+\s(?P<func>\S+)\(..\)\sreferences\s(EPERM|EACCES)$")

# https://regex101.com/r/4WqbAq/1
namespace_re = re.compile(
    r"^(?P<file>\S+):\d+:\d+\s(?P<func>\S+)\(..\)\sreferences\s(CLONE_\w+)$")
scores = {}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="This script scores functions profiled by our custom Smatch 'check_access_control' analyzer. Every time a function references either EACCES or EPERM, its score goes up by one.")
    parser.add_argument("search", help="Specify what type of macro to search for. 'access_ctl' counts occurrences of EACCES and EPERM. 'namespace' counts occurrences of 'CLONE_*'", choices=['access_ctl', 'namespace'])
    parser.add_argument("smatch_log", help="Path to the Smatch output log. Must contain logs from the check_access_control analyzer.")
    args = parser.parse_args()

    if args.search == 'access_ctl':
        requested_re = access_ctl_re
    elif args.search == 'namespace':
        requested_re = namespace_re

    f = open(args.smatch_log, 'r')
    for line in f:
        match = requested_re.match(line)
        if match:
            func = match.groupdict()['func']
            _file = match.groupdict()['file']
            if (_file, func) not in scores:
                scores[(_file, func)] = 0

            scores[(_file, func)] += 1

    score_json = []
    for (_file, func), score in scores.iteritems():
        score_json.append({
            "file": _file,
            "func": func,
            "score": score
        })
    print json.dumps(score_json)
