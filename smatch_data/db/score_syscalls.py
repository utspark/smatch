#!/usr/bin/python

# Copyright (C) 2013 Oracle.
#
# Licensed under the Open Software License version 1.1

import sqlite3
import sys
import re
import os
import json
import multiprocessing


SYSCALL_PREFIX = '__se_sys_'
visited = {}
results = set()
syscall_list_path = "%s/../../smatch_scripts/systemcall_list.txt" % os.path.dirname(os.path.realpath(__file__))
syscalls = [('{0}{1}'.format(SYSCALL_PREFIX, line)).strip() for line in open(syscall_list_path, 'r')]


function_ptrs = []
searched_ptrs = []
def get_function_pointers_helper(func, con):
    cur = con.cursor()
    cur.execute("select distinct ptr from function_ptr where function = '%s';" %(func))
    for row in cur:
        ptr = row[0]
        if ptr in function_ptrs:
            continue
        function_ptrs.append(ptr)
        if not ptr in searched_ptrs:
            searched_ptrs.append(ptr)
            get_function_pointers_helper(ptr, con)


def get_function_pointers(func, con):
    global function_ptrs
    global searched_ptrs
    function_ptrs = [func]
    searched_ptrs = [func]
    get_function_pointers_helper(func, con)
    return function_ptrs


def get_callers(func, _file, con):
    """
        Get a list of functions that call a requested function.

        @param func: The name of the callee function.
        @param _file: The path of the file the function resides.
        @param con: The database connection object.
    """
    ret = []
    cur = con.cursor()
    ptrs = get_function_pointers(func, con)
    for ptr in ptrs:
        query = "select distinct caller, file, static from caller_info where function = '%s'" % ptr
        cur.execute(query)
        rows = cur.fetchall()
        if any([int(row[2])==1 for row in rows]):
            # If static, remove all entries with the wrong file
            rows = filter(lambda row: row[1]==_file, rows)
        for row in rows:
            ret.append((row[0], row[1]))
    return ret


def upstream_syscall_helper(_file, func, con, max_depth=None):
    """
        Perform a breadth-first search up the backwards slice of a function's
        call graph until we reach a system call. Once we hit a system call we
        will stop our search.

        @param _file: The path of the file the function resides in.
        @param func: The name of the function.
        @param con: Database connection object.
        @param depth: The number of calls deep to search.
    """
    if func in syscalls:
        results.append(func.replace(SYSCALL_PREFIX, ''))
        return

    prev_explored = {} # Quickly skip previously explored function
    depth = 0 # Keep track of our tree level
    found = {} # Map of found system calls

    caller_list = get_callers(func, _file, con)
    caller_list.append(('end', '')) # Mark the end of this tree level

    for caller, caller_file in caller_list:

        if max_depth is not None and depth > max_depth:
            break

        if caller == 'end':
            depth += 1
            continue

        if caller in prev_explored:
            continue

        if caller in syscalls:
            syscall_name = caller.replace(SYSCALL_PREFIX, '')
            if syscall_name not in found:
                found[syscall_name] = (caller, caller_file)
                continue

        caller_list += get_callers(caller, caller_file, con)
        caller_list.append(('end', ''))
        prev_explored[caller] = (caller, caller_file)
    return found.keys()


def jsonfile_to_scoresheet(jsonfile):
    scoresheet = {}
    with open(jsonfile, 'r') as f:
        data = json.load(f)
    for item in data:
        _file = str(item['file'])
        func = str(item['func'])
        score = int(item['score'])
        assert (_file, func) not in scoresheet,\
            "This file and function appear twice: %s:%s --  must be unique." %(_file, func)
        scoresheet[(_file, func)] = score
    return scoresheet


def score_syscalls(func_scores, max_depth=30):
    """
        For each function provided, find all system calls that can reach the
        function and percolate the functions score to the system call.

        @param func_score: Dictionary of {(file, func): score, ...}.
        @param max_depth: Maximum depth up the tree to search for a syscall.
    """

    def _do_score(func_list, out_file, max_depth):
        """ Small function for doing the system call scoring for each function. """

        try:
            con = sqlite3.connect('smatch_db.sqlite')
        except sqlite3.Error, e:
            print "Error %s:" % e.args[0]
            sys.exit(1)

        syscall_score = {}
        for _file, func in func_list:
            print "Scoring syscalls upstream of %s:%s ..." % (_file, func)

            results = upstream_syscall_helper(_file, func, con, max_depth=max_depth)
            for syscall in results:
                if syscall not in syscall_score:
                    syscall_score[syscall] = 0
                syscall_score[syscall] += func_scores[(_file, func)]

        with open(out_file, 'w') as f:
            json.dump(syscall_score, f)
        

    # Create a process for each cpu core and assign it a list of functions.
    # Divide the list evenly since we don't know ahead of time how much work
    # each system call will be.
    cpu_count = multiprocessing.cpu_count()
    p_list = []
    func_list = func_scores.keys()
    fraction = len(func_list)/cpu_count
    for i in range(0, cpu_count):

        start = fraction * i
        if (i + 1) == multiprocessing.cpu_count():
            end = len(func_list)
        else:
            end = start + fraction

        p = multiprocessing.Process(target=_do_score, args=(func_list[start:end], 'scores{0}.json'.format(i), max_depth)) 
        p.start()
        p_list.append(p)

    for p in p_list:
        p.join()

    # Aggregate the scores calculated by our processes.
    scores = {}
    for i in range(0, cpu_count):
        with open('./scores{0}.json'.format(i), 'r') as f:
            tmp_score = json.load(f)

        for syscall, score in tmp_score.iteritems():
            if syscall not in scores:
                scores[syscall] = score
            else:
                scores[syscall] += score


    with open('scores.txt', 'w') as f:
        for key, value in sorted(scores.iteritems(), key=lambda (k,v): (v,k)):
            f.write('{0}: {1}\n'.format(key, value))
    return


if __name__ == '__main__':
    func_scores_json = sys.argv[1]
    func_scores = jsonfile_to_scoresheet(func_scores_json)
    score_syscalls(func_scores)
